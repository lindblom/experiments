var data = [
    { name: 'horse', image: '' },
    { name: 'hen', image: '' },
    { name: 'monkey', image: '' },
    { name: 'hamster', image: '' },
    { name: 'squirl', image: '' },
    { name: 'dolphin', image: '' },
    { name: 'cat', image: '' }
];

var searchField;
var suggestionList;
var filteredList = [];
var selected = -1;

function inputListener(e) {
    if (e.keyCode == 27) {
        selected = -1;
        filteredList = [];
        render(filteredList);
        return;
    }

    if (e.keyCode == 38) {
        moveSelected(-1, e);
        return;
    } else if (e.keyCode == 40) {
        moveSelected(1, e);
        return;
    }

    if (e.keyCode == 13 && selected != -1) {
        var name = filteredList[selected].name;
        setTimeout(function() {
            alert(name);
        }, 20);
        e.target.value = '';
    }

    setTimeout(function() {
        selected = -1;
        filteredList = filterList(e.srcElement.value.toLowerCase());
        render(filteredList);
    }, 10);
}

function moveSelected(delta, e) {
    e.preventDefault();
    selected = selected + delta;
    if(selected < -1) selected = -1;
    if(selected > filteredList.length - 1) selected = filteredList.length - 1;
    render(filteredList);
}

function render(suggestions) {
    var html = [];

    suggestions.forEach(function (e, i) {
        var li = '<li>'

        if (selected == i)
            li = '<li class="selected">';

        html.push(li + e.name + '</li>');
    });

    suggestionList.innerHTML = html.join('');
}

function filterList(word) {

    if (word.length == 0) return [];

    return data.filter(function(e) {
        return e.name.toLowerCase().indexOf(word) === 0;
    });
}

function init() {
    suggestionList = document.getElementById('suggestionList');
    searchField = document.getElementById('field');
    searchField.addEventListener('keydown', inputListener);
}

window.onload = init;
