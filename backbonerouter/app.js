
var Root = React.createClass({
    bye: function () {
        router.navigate('bye', {trigger: true});
    },
    render: function () {
        return React.DOM.p({},
            'Hello', React.DOM.button({onClick: this.bye}, 'Say bye!'));
    }
});

var Bye = React.createClass({
    hello: function () {
        router.navigate('', {trigger: true});
    },
    render: function () {
        return React.DOM.p({}, 'Bye',
            React.DOM.button({onClick: this.hello}, 'Say hello!'));
    }
});


var App = React.createClass({
    render: function () {
        switch (this.props.route) {
        case '':
            return Root();
        case 'bye':
            return Bye();
        }
    }
});
