var router = new (Backbone.Router.extend({
    routes: {
        '': 'root',
        'bye': 'bye'
    },
    root: function () {
        React.renderComponent(App({route: ''}), document.body);
    },
    bye: function () {
        React.renderComponent(App({route: 'bye'}), document.body);
    }
}))();

Backbone.history.start({pushState: true});
