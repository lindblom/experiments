var data = ['annelie', 'christopher', 'olivia', 'oliver', 'anders', 'agda',
            'anna', 'charles', 'charlie', 'anna-lena'];

var SuggestionList = React.createClass({
    render: function () {
        var i = 0;
        return React.DOM.ul({},
            this.props.suggestions.map(function (name) {
                var attr = {key: name};
                if (i++ === this.props.selected) attr.className = 'selected';
                return React.DOM.li(attr, name);
            }.bind(this))
        );
    }
});

var SearchField = React.createClass({
    render: function () {
        return React.DOM.input({
            type: 'text',
            value: this.props.value,
            onChange: this.props.userInput,
            onKeyDown: this.props.userMove
        });
    }
});

var SearchForm = React.createClass({
    getInitialState: function () {
        return { value: '', suggestions: [], selected: -1 };
    },
    userInput: function (event) {
        this.setState({
            value: event.target.value,
            suggestions: this.findSuggestions(event.target.value),
            selected: -1
        });
    },
    userMove: function (event) {
        if (event.keyCode === 38 && this.state.selected !== -1) {
            this.setState({selected: this.state.selected - 1});
            return false;
        }

        if (event.keyCode === 40 && this.state.selected < this.state.suggestions.length - 1) { //down
            this.setState({selected: this.state.selected + 1});
            return false;
        }

        if (event.keyCode == 27) {
            this.setState({selected: -1, suggestions: []});
            return false;
        }

        if (event.keyCode == 13 && this.state.selected !== -1) {
            alert(this.state.suggestions[this.state.selected]);
            return false;
        }
    },
    findSuggestions: function (value) {

        if (value.length === 0) return [];

        return this.props.data.filter(function (name) {
            return name.toLowerCase().indexOf(value.toLowerCase()) === 0;
        });
    },
    render: function () {
        return React.DOM.div({},
            SearchField({ value: this.state.value, userInput: this.userInput, userMove: this.userMove }),
            SuggestionList({ suggestions: this.state.suggestions, selected: this.state.selected })
        );
    }
});

React.renderComponent(SearchForm({data: data}), document.body);
