var socket = io.connect('http://localhost');

var LoginBox = React.createClass({
    displayName: 'LoginBox',
    onSubmit: function (e) {
        this.props.login(this.refs.username.state.value);
    },
    render: function () {
        return React.DOM.div({},
            React.DOM.input({type: 'text', ref: 'username'}),
            React.DOM.button({onClick: this.onSubmit}, 'Submit')
        );
    }
});

var ChatInput = React.createClass({
    displayName: 'ChatInput',
    sendMessage: function () {
        if (this.refs.message.state.value.lenght !== 0) {
            this.props.sendMessage(this.refs.message.state.value);
            this.refs.message.state.value = '';
        }
    },
    render: function () {
        return React.DOM.div({},
            React.DOM.input({type: 'text', ref: 'message'}),
            React.DOM.button({onClick: this.sendMessage}, 'Send')
        );
    }
});

var ChatMessages = React.createClass({
    displayName: 'ChatMessages',
    render: function () {
        return React.DOM.div({},
            this.props.messages.map(function (message) {
                return React.DOM.div({},
                    React.DOM.span({className: 'user'}, message.user + ' '),
                    React.DOM.span({className: 'message'}, message.message)
                );
            })
        );
    }
});

var ChatBox = React.createClass({
    displayName: 'ChatBox',
    getInitialState: function () {
        return { messages: [] };
    },
    componentDidMount: function () {
        socket.on('newMessage', this.incommingMessage);
    },
    sendMessage: function (message) {
        socket.emit('chatMessage', {user: this.props.user, message: message});
    },
    incommingMessage: function (data) {
        this.state.messages.push(data);
        this.forceUpdate();
    },
    render: function () {
        return React.DOM.div({},
            ChatInput({sendMessage: this.sendMessage}),
            ChatMessages({messages: this.state.messages})
        );
    }
});

var ChatContainer = React.createClass({
    displayName: 'ChatContainer',
    getInitialState: function () {
        return {user: null};
    },
    login: function (username) {
        this.setState({user: username});
    },
    render: function () {
        if (!this.state.user) {
            return LoginBox({login: this.login});
        } else {
            return ChatBox({user: this.state.user});
        }
    }
});

React.renderComponent(ChatContainer(), document.body);
