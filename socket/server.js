var app = require('http').createServer(handler),
    io = require('socket.io').listen(app),
    fs = require('fs');

app.listen(3000);

function handler (req, res) {
    if (req.url === '/app.js') return appJs(req, res);

  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

function appJs(req, res) {
    fs.readFile(__dirname + '/app.js',
    function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading app.js');
      }

      res.writeHead(200);
      res.end(data);
    });
}

io.sockets.on('connection', function (socket) {
    socket.on('chatMessage', function (data) {
        io.sockets.emit('newMessage', data);
    });
});
